## Welcome to myfridge4

### A RESTful API example written in Clojure.

Check out the live Swagger [API Docs](/api/api-docs/)

See the source [myfridge4](https://gitlab.com/tmarble/myfridge4)
