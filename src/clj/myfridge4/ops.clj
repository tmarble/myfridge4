(ns myfridge4.ops
  (:require [clojure.spec.alpha :as s]
            [myfridge4.specs :as specs]))

(defn success
  ([]
   (success nil nil))
  ([results]
   (success results nil))
  ([results message]
   (cond-> {:status 200}
     message (assoc :message message)
     results (assoc :results results))))

(defn bad-request [message]
  {:status 400
   :message message})

(defn not-found [message]
  {:status 404
   :message message})

;; math API

;; (defn plus [x y]
;;   (let [total (+ x y)]
;;     (if (< total 1)
;;       (bad-request "plus operations must yield a positive total")
;;       (success total))))

;; (s/fdef plus
;;   :args (s/cat :x ::specs/x :y ::specs/y)
;;   :ret  ::specs/plus-return
;;   :fn   (s/or :bad-request #(= 400 (-> % :ret :status second))
;;               :ok (s/and #(= 200 (-> % :ret :status second))
;;                          #(pos? (-> % :ret :results))
;;                          #(= (-> % :ret :results)
;;                              (+ (-> % :args :x) (-> % :args :y))))))

;; in memory storage implementation

(def fridge (atom {}))

(defn fridge-insert [name item]
  (swap! fridge assoc name item))

(defn fridge-select
  ([] ;; all items
   (vec (vals @fridge)))
  ([name]; ;; named item
   (get @fridge name)))

(defn fridge-alter [name item]
  (fridge-insert name item))

(defn fridge-remove [name]
  (swap! fridge assoc name nil))

;; fridge API

(defn fridge-create [{:keys [name desc qty units]}]
  (let [desc (if (empty? desc) name desc) ;; desc defaults to name
        qty (or qty 1) ;; qty defaults to 1
        units (or units "count") ;; units defaults to count
        item (fridge-select name)]
    (if item
      (bad-request (str "fridge already has item: " name))
      (let [item {:name name
                  :desc desc
                  :qty qty
                  :units units}]
        (fridge-insert name item)
        (success)))))

(s/fdef fridge-create
  :args (s/cat :body ::specs/fridge-create)
  :ret  ::specs/fridge-create-return)

(defn fridge-read [{:keys [name]}]
  (if (empty? name)
    (success (fridge-select)) ;; all items
    (let [item (fridge-select name)]
      (if-not item
        (not-found (str "fridge does not have item: " name))
        (success [item])))))

(s/fdef fridge-read
  :args (s/cat :body ::specs/fridge-read)
  :ret  ::specs/fridge-read-return)

(defn fridge-update [{:keys [name desc qty units]}]
  (let [item (fridge-select name)]
    (if-not item
      (not-found (str "fridge does not have item: " name))
      (let [item {:name name
                  :desc (or desc (:desc item))
                  :qty (or qty (:qty item))
                  :units (or units (:units item))}]
        (fridge-alter name item)
        (success)))))

(s/fdef fridge-update
  :args (s/cat :body ::specs/fridge-update)
  :ret  ::specs/fridge-update-return)

(defn fridge-delete [{:keys [name]}]
  (let [item (fridge-select name)]
    (if-not item
      (not-found (str "fridge does not have item: " name))
      (do
        (fridge-remove name)
        (success)))))

(s/fdef fridge-delete
  :args (s/cat :body ::specs/fridge-delete)
  :ret  ::specs/fridge-delete-return)
