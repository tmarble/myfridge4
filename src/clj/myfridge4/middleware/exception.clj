(ns myfridge4.middleware.exception
  (:require [clojure.tools.logging :as log]
            [clojure.tools.logging :as log]
            [expound.alpha :as expound]
            [reitit.coercion :as coercion]
            [reitit.ring.middleware.exception :as exception]
            [myfridge4.layout :refer [error-json]]))

(defn coercion-error-handler [status]
  (let [printer (expound/custom-printer {:print-specs? false})]
    (fn [exception request]
      (error-json {:status 400
                   :message (with-out-str
                              (printer (-> exception ex-data :problems)))}))))

(defn http-response-handler
  "Reads response from Exception ex-data :response"
  [e _]
  (error-json {:status 500
               :message (-> e ex-data :response)}))

(defn request-parsing-handler [e _]
  (error-json {:status 400
               :message (str "Malformed "
                             (-> e ex-data :format pr-str) " request.")}))

(def exception-middleware
  (exception/create-exception-middleware
   (merge
    exception/default-handlers
    {;; override
     :reitit.ring/response http-response-handler
     :muuntaja/decode request-parsing-handler
     ;; log stack-traces for all exceptions -- very verbose
     ;; ::exception/wrap (fn [handler e request]
     ;;                    (log/debug "=== WRAP BEGIN ===")
     ;;                    (log/error e (.getMessage e))
     ;;                    (log/debug "=== WRAP END ===")
     ;;                    (handler e request))
     ;; human-optimized validation messages
     ::coercion/request-coercion (coercion-error-handler 400)
     ::coercion/response-coercion (coercion-error-handler 500)})))
