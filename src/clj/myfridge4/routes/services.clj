(ns myfridge4.routes.services
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [clojure.tools.logging :as log]
            [cheshire.core :as json]
            [ring.util.http-response :refer :all]
            [reitit.swagger :as reitit-swagger]
            [reitit.swagger-ui :as swagger-ui]
            [reitit.ring.coercion :as ring-coercion]
            [reitit.coercion.spec :as spec-coercion]
            [reitit.ring.middleware.muuntaja :as muuntaja]
            [reitit.ring.middleware.parameters :as parameters]
            [myfridge4.specs :as specs]
            [myfridge4.ops :as ops]
            [myfridge4.middleware.formats :as formats]
            [myfridge4.middleware.exception :as exception]))

;; response has status, optionally message, results
(defn respond [response]
  (let [{:keys [status message results]} response]
    {:status status
     :headers {"Content-Type" "application/json"}
     :body (json/generate-string
            (cond-> {:status status}
              message (assoc :message message)
              results (assoc :results results))
           {:escape-non-ascii true})}))

(defn log-request-handler [handler]
  (fn [req]
    (let [v (handler req)]
      (log/info "REQUEST" (:request-method req) (:uri req)
                "FROM" (:remote-addr req)
                "=> RESPONSE" (:status v))
      v)))

(def log-request-middleware
  {:name ::log-request
   :wrap log-request-handler})

(defn description
  "returns description of this service"
  []
  (let [version-url (io/resource "version.txt")
        version (if version-url (slurp version-url) "unknown")
        commit (if (or (= version "unknown")
                       (< (-> version string/split-lines count) 4))
                 version
                 (-> version string/split-lines (nth 3)))
        repo-url "https://gitlab.com/tmarble/myfridge4"
        commit-url (if (re-matches #"\p{XDigit}+" commit) ;; SHA?
                     (str repo-url "/commit/" commit))
        modified (if version-url
                   (java.util.Date. (.getLastModified
                                     (.openConnection version-url)))
                   "unknown")]

    (str "RESTful API example in Clojure <a href=\"/api/swagger.json\">swagger.json</a><br/>"
         (if commit-url "commit " "version ")
         (if commit-url (str "<a href=\"" commit-url "\">"))
         commit
         (if commit-url "</a>")
         " built at " modified)))

(defn service-routes []
  ["/api"
   {:coercion spec-coercion/coercion
    :muuntaja formats/instance
    :swagger {:id ::api}
    :middleware [log-request-middleware
                 ;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 exception/exception-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 ring-coercion/coerce-response-middleware
                 ;; coercing request parameters
                 ring-coercion/coerce-request-middleware]}

   ;; swagger documentation
   ["" {:no-doc true
        :swagger {;; :basePath "/api"
                  :info {:title "myfridge4"
                         :version "1.0.0"
                         :description (description)
                         :contact {:name "Tom Marble"
                                   :email "tmarble@gmail.com"
                                   :url "https://gitlab.com/tmarble/myfridge4"}
                         :license {:name "Apache License 2.0"
                                   :url "http://opensource.org/licenses/Apache-2.0"}}
                  }
        }

    ["/swagger.json"
     {:get (reitit-swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
             {:url "/api/swagger.json"
              :config {:validator-url nil}})}]]

   ["/ping"
    {:get {:summary "simple ping test"
           :responses {200 {:body ::specs/ping-response}}
           :handler (constantly (ok {:message "pong"}))}}]

   ;; ["/math"
   ;;  {:swagger {:tags ["math"]}}

   ;;  ["/plus"
   ;;   {:get {:summary "plus with query parameters"
   ;;          :parameters {:query ::specs/plus-params}
   ;;          :handler (fn [{{{:keys [x y] :or {y 100}} :query} :parameters}]
   ;;                     (respond (ops/plus x y)))}

   ;;    :post {:summary "plus with body parameters"
   ;;           :parameters {:body ::specs/plus-params}
   ;;           :handler (fn [{{{:keys [x y] :or {y 100}} :body} :parameters}]
   ;;                      (respond (ops/plus x y)))}
   ;;    }]]

   ["/fridge"
    {:swagger {:tags ["fridge"]}
     :post {:summary "create fridge item"
            :parameters {:body ::specs/fridge-create}
            :responses {200 {:body ::specs/json-string}}
            :handler (fn [{{params :body} :parameters}]
                       (respond (ops/fridge-create params)))}

     :get {:summary "read fridge item"
           :parameters {:query ::specs/fridge-read}
           :responses {200 {:body ::specs/json-string}}
           :handler (fn [{{params :query} :parameters}]
                      (respond (ops/fridge-read params)))}

     :put {:summary "update fridge item"
           :parameters {:body ::specs/fridge-update}
           :responses {200 {:body ::specs/json-string}}
           :handler (fn [{{params :body} :parameters}]
                      (respond (ops/fridge-update params)))}

     :delete {:summary "delete fridge item"
              :parameters {:body ::specs/fridge-delete}
              :responses {200 {:body ::specs/json-string}}
              :handler (fn [{{params :body} :parameters}]
                         (respond (ops/fridge-delete params)))}}]])
