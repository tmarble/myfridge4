(ns myfridge4.specs
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]))

;; helping functions

(defn abs
  "Returns absolute value"
  [a]
  (if (neg? a) (- a) a))

(defn gen-nat []
  (gen/fmap abs (gen/int)))

(defn gen-pos-int []
  (gen/fmap inc (gen-nat)))

(s/def ::natural
  (s/with-gen (s/and int? (s/or :pos pos? :zero zero?))
    #(gen-nat)))

;; is s a non-empty alphanumeric string?
(defn alphanumeric? [s]
  (boolean
   (and (string? s)
        (re-matches #"[A-Za-z0-9]+" s))))

(defn gen-alphanumeric []
  (gen/fmap
   (fn [a]
     (if (empty? a)
       (str (gen/char-alphanumeric))
       a))
   (gen/string-alphanumeric)))

(s/def ::alphanumeric
  (s/with-gen (s/and string? alphanumeric?)
    #(gen-alphanumeric)))

(s/def ::status ::natural)

(s/def ::message string?)

(s/def ::response
  (s/with-gen
    (s/keys :req-un [::status]
            :opt-un [::message])
    #(gen/return {:status 200})))

(s/def ::json-string string?)

;; ping API

(s/def ::ping-response
  (s/with-gen
    (s/keys :req-un [::message])
    #(gen/return {:message "pong"})))

;; math API

;; (s/def ::integer int?)

;; (s/def ::x ::integer)

;; (s/def ::y ::integer)

;; (s/def ::plus-params
;;   (s/with-gen
;;     (s/keys :req-un [::x]
;;             :opt-un [::y])
;;     #(gen/map
;;       (gen/frequency [[3 (gen/return :x)] [2 (gen/return :y)]])
;;       (gen/int))))

;; (s/def ::plus-return ::response)

;; fridge API

(s/def ::name ::alphanumeric)

(s/def ::desc string?)

(s/def ::qty ::natural)

(def units #{"count" "ounces" "slices"})

(s/def ::units units)

(s/def ::fridge-name
  (s/with-gen
    (s/keys :req-un [::name])
    #(gen/map
      (gen/return :name)
      (gen/string-alphanumeric))))

(s/def ::fridge-item
  (s/with-gen
    (s/keys :req-un [::name]
            :opt-un [::desc ::qty ::units])
    #(gen/fmap
      (fn [n]
        (cond-> {:name (gen/generate (s/gen ::name))}
          (even? n) (assoc :desc  (gen/generate (s/gen ::desc)))
          (bit-and 0x4 n)  (assoc :qty   (gen/generate (s/gen ::qty)))
          (bit-and 0x8 n) (assoc :units (gen/generate (s/gen ::units)))))
      (gen/choose 1 15))))

(s/def ::fridge-create ::fridge-item)

(s/def ::fridge-create-return ::response)

(s/def ::fridge-read
  (s/with-gen
    (s/keys :opt-un [::name])
    #(gen/fmap
      (fn [n]
        (cond-> {}
          (even? n) (assoc :name (gen/generate (s/gen ::name)))))
      (gen-nat))))

(s/def ::fridge-read-return ::response)
  ;; (s/coll-of ::fridge-item :kind vector?))

(s/def ::fridge-update ::fridge-item)

(s/def ::fridge-update-return ::response)

(s/def ::fridge-delete ::fridge-name)

(s/def ::fridge-delete-return ::response)
