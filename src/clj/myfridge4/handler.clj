(ns myfridge4.handler
  (:require [clojure.string :as string]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.webjars :refer [wrap-webjars]]
            [reitit.swagger-ui :as swagger-ui]
            [reitit.ring :as ring]
            [myfridge4.env :refer [defaults]]
            [myfridge4.middleware :as middleware]
            [myfridge4.layout :refer [error-html error-json]]
            [myfridge4.routes.home :refer [home-routes]]
            [myfridge4.routes.services :refer [service-routes]]
            [mount.core :as mount]))

(mount/defstate init-app
  :start ((or (:init defaults) (fn [])))
  :stop  ((or (:stop defaults) (fn []))))

(defn error [error-details]
  (fn [& args]
    (let [uri (or (:uri (first args)) "")]
      (if (string/starts-with? uri "/api")
        (error-json error-details) ;; respond to API errors in JSON
        (error-html error-details))))) ;; else in HTML

(mount/defstate app-routes
  :start
  (ring/ring-handler
   (ring/router
    [(home-routes)
     (service-routes)])
   (ring/routes
    (ring/create-resource-handler
     {:path "/"})
    (wrap-content-type
     (wrap-webjars (constantly nil)))
    (ring/create-default-handler
     {:not-found
      (error {:status 404, :title "404 - Page not found"})
      :method-not-allowed
      (error {:status 405, :title "405 - Not allowed"})
      :not-acceptable
      (error {:status 406, :title "406 - Not acceptable"})}))))

(defn app []
  (middleware/wrap-base #'app-routes))
