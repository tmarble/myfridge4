#!/bin/sh
# all-tests

program=$(basename $0)
dir=$(dirname $0)
cd "$dir/../.."
CODE="$(pwd -P)"
project=$(basename $CODE)
RESULTS="$CODE/target/cli-test"
mkdir -p "$RESULTS"
log="$RESULTS/$program.log"
rm -f "$log"

set -e

echo "-- running all tests for $project --"
if [ ! -e "test-config.edn" ]; then
    echo "could not find the file: test-config.edn"
    echo "did you copy the default configuration files?"
    echo "  cp resources/config/{dev,test}-config.edn ./"
    echo
    echo "Tests failed."
    exit 1
fi

echo
echo "-- lein test -- "
# run the Clojure tests
lein test

echo
echo "-- CLI test -- "
# start the project to run the CLI tests
printf "starting $project"
pid=0
cleanup() {
  if kill -0 $pid > /dev/null 2>&1 ; then
      pids=$(jps -l | awk "/clojure.main/ { print \$1; }")
      if [ -n "$pids" ]; then
          echo
          echo "stopping $project"
          kill $pids
      fi
  fi
}
trap cleanup 0 1 2 3 15

lein run > "$log" 2>&1 &
# NOTE leiningen will exec a couple times so this pid is not actually java
pid=$!

# wait until the server is up...
i=0
max=60
while [ $i -lt $max ]; do
    sleep 1
    if grep 'http-server started' "$log" > /dev/null; then
        echo
        echo
        break
    fi
    printf .
    i=$((i + 1))
done

./test/bin/cli-test

exit 0
