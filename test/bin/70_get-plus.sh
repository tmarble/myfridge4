#!/bin/sh
# get-plus

set -e

api="$URL/api/math/plus"

# construct curl args
args=""
args="$args -s" # NOTE turning off silent mode is useful for debugging
args="$args -m 5" # don't wait more than 5 seconds

accept="Accept: application/json"

params="?x=2"

# disabled for now
# curl $args --header "$accept" "$api$params"
