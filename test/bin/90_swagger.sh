#!/bin/sh
# swagger

set -e

api="$URL/api/swagger.json"

# construct curl args
args=""
args="$args -s" # NOTE turning off silent mode is useful for debugging
args="$args -m 5" # don't wait more than 5 seconds

accept="Accept: application/json"

curl $args --header "$accept" $api

# ensure the output ends with a newline
echo
