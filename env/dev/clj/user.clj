(ns user
  "Userspace functions you can run by default in your local REPL."
  (:require
    [myfridge4.config :refer [env]]
    [clojure.spec.alpha :as s]
    [expound.alpha :as expound]
    [mount.core :as mount]
    [myfridge4.core :refer [start-app]]
    [clojure.spec.alpha :as s]
    [clojure.spec.gen.alpha :as gen]
    [clojure.spec.test.alpha :as stest]))

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(add-tap (bound-fn* clojure.pprint/pprint))

(defn start
  "Starts application.
  You'll usually want to run this on startup."
  []
  (mount/start-without #'myfridge4.core/repl-server))

(defn stop
  "Stops application."
  []
  (mount/stop-except #'myfridge4.core/repl-server))

(defn restart
  "Restarts application."
  []
  (stop)
  (start))
