# myfridge4

A RESTful API example written in Clojure

What's in the fridge? You can find out by using the API (see _Try it Live_ below)

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

You can copy the dev and test configurations (which are NOT
checked into the top level directory)

    cp resources/config/{dev,test}-config.edn ./

## Running

To start a web server for the application, run:

    lein run

You can then view the website at:

    open http://localhost:3000

## Tests

The standard Clojure tests are run like this:

    lein test

The CLI tests (using curl) are run like this:

    ./test/bin/cli-test

## Try it live!

Try the example site on [heroku](https://cryptic-bayou-86886.herokuapp.com/)

## Feature Summary

The [Luminus Framework](http://www.luminusweb.net/docs/guestbook.html#anatomy_of_a_luminus_application) provides the basic infrastructure.

### Implemented Features

* Hosted on GitLab
* Logs errors and requests
  + http://www.luminusweb.net/docs/logging.html
  + https://devcenter.heroku.com/articles/papertrail
* Deployment to [heroku](https://cryptic-bayou-86886.herokuapp.com/)
  + https://devcenter.heroku.com/articles/clojure-support
  + https://devcenter.heroku.com/articles/deploying-clojure-applications-with-the-heroku-leiningen-plugin
  + https://github.com/heroku/clojure-getting-started
* clojure.spec property based test coverage
  + https://clojure.org/about/spec
  + https://clojure.org/guides/spec
* CI/CD
  + https://docs.gitlab.com/ee/ci/
  + https://gitlab.com/help/topics/autodevops/index.md

### Future Features

* Implement a datastore
  + PostgresQL http://www.luminusweb.net/docs/database.html
  + Migration framework http://www.luminusweb.net/docs/migrations.html
  + https://github.com/luminus-framework/conman
  + https://github.com/layerware/hugsql
* Code Coverage
  + https://docs.codeclimate.com/docs/kibit
  + http://www.luminusweb.net/docs/profiles.html#miscellaneous
* Authorization
  + http://www.luminusweb.net/docs/security.html
  + https://funcool.github.io/buddy-auth/latest/#example-jwe
  + https://jwt.io/introduction/
* Metrics
  + https://corfield.org/blog/2013/05/01/instrumenting-clojure-for-new-relic-monitoring/
* Rate Limiting
  + https://github.com/killme2008/clj-rate-limiter

## Copyright and license

Copyright (c) 2019 Tom Marble

Licensed under the [Apache License 2.0](http://opensource.org/licenses/Apache-2.0) [LICENSE](LICENSE)
